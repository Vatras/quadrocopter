/**
 * Created by pjesek on 19.10.17.
 */

function checkInput(config){
    var transmitters = config.transmitters;
    function isPointInside(point,circle){
        return ((Math.pow(point.x-circle.x,2) + (Math.pow(point.y-circle.y,2)) <= (Math.pow(circle.power,2))))
    }
    function findTransmitterCoveringThePoint(point){
        return transmitters.find(function(transmitter){
           return isPointInside(point,transmitter)
        })
    }
    function createGraph(){
        transmitters = transmitters.map(function(transmitter,idx){
            transmitter.idx = idx
            return transmitter;
        });
        var graph = {};
        transmitters.forEach(function(t1,idx1){
            transmitters.forEach(function(t2,idx2){
                if(isIntersection(t1,t2) && idx1!=idx2){
                    graph[idx1] = graph[idx1] || [];
                    graph[idx1].push(t2);
                }
            })
        })
        return graph;
    }

    function isIntersection(c1,c2){
        var distanceSquared = Math.pow((c1.x-c2.x),2) + Math.pow((c1.y-c2.y),2)
        return ((Math.pow(c1.power-c2.power,2) <= distanceSquared) && (distanceSquared <= (Math.pow(c1.power+c2.power,2))))
    }
    var status=false;
    function isInArray(arr,item){
        return arr.indexOf(item)>-1;
    }
    function getNotVisited(arr,neighbours){
        var toVisit = neighbours.filter(function(x){return arr.indexOf(x) == -1});
        return toVisit.length===0 ? null : toVisit;
    }

    function findWayBetweenStartAndEnd(graph,start,end){
        var visited = [start.idx];
        function iterate(iterator){
            if(status){return}
            console.log("iterator",iterator)
            var neighbours = graph[iterator] || [];
            neighbours=neighbours.map(function (item) {
                return item.idx
            });
            if(isInArray(neighbours,end.idx)){
                status = true;
            }else{
                var notVisited = getNotVisited(visited,neighbours);

                if(notVisited){
                    notVisited.forEach(function(val){
                        visited.push(val)
                        iterate(val);
                    })
                }
            }
        }
        console.log(graph[start.idx]);
        iterate(start.idx,visited,end);
    }
    function existsPolygon(){
        var start = findTransmitterCoveringThePoint(config.startPoint);
        var end = findTransmitterCoveringThePoint(config.endPoint);
        if(!end || !start){
            return false;
        }
        var graph = createGraph()
        findWayBetweenStartAndEnd(graph,start,end);
        return status;
    }
    var result = existsPolygon();
    return result;
}
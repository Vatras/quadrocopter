/**
 * Created by pjesek on 19.10.17.
 */
var defaultConfig = {
    "transmitterNum" : 6,
    "startPoint":
    {
        "x" : 10,
        "y" : 19
    },
    "endPoint":
    {
        "x" : 19,
        "y" : 11
    },
    "transmitters" : [
        {
            "x" : 6,
            "y" : 11,
            "power" : 4
        },
        {
            "x" : 8,
            "y" : 17,
            "power" : 3
        },
        {
            "x" : 19,
            "y" : 19,
            "power" : 2
        },
        {
            "x" : 19,
            "y" : 11,
            "power" : 4
        },
        {
            "x" : 15,
            "y" : 7,
            "power" : 6
        },
        {
            "x" : 12,
            "y" : 19,
            "power" : 4
        }
    ]
}
document.getElementById('inputData').value=JSON.stringify(defaultConfig,null,2);

document.getElementById('submitBtn').addEventListener('click',function(){
    var input = document.getElementById('inputData').value;
    var config = JSON.parse(input);
    var res = checkInput(config) ? "Safe passage is possible" : "Safe passage is not possible";
    document.getElementById('resultField').innerHTML = res;
});